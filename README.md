# ModuleCommandParser

Программа для перевода текстовых команд в бинарные скрипты

## Запуск

Для сборки и запуска используется **.NET 6**

```console
dotnet ModuleCommandParser.dll <path-to-input-file> [output-dir]
```

Если `output-dir` будет пустым, то в качестве выходной директории будет создана директория output о пути входного файла

## Доступные команды

| Команда         | Синонимы | Описание                                          | Аргумент            |
|:----------------|:---------|:--------------------------------------------------|:--------------------|
| `rotate1`       | `r1`     | Повернуть Q1 на угол                              | float               |
| `rotate2`       | `r2`     | Повернуть Q2 на угол                              | float               |
| `connect`       | `cn`     | Послать сигнал на платформы присоединения         | ConnectionInfo      |
| `block`         | `b`      | Конец блока команд                                | -                   |
| `repeate`       | `loop`   | Повторять команды ниже                            | -                   |
| `configuration` | `conf`   | Название конфигурации/скрипта                     | string (одно слово) |
| `id`            | `module` | Блок команд для модуля с заданным идентификатором | byte                |

Аргумент `ConnectionInfo` представляет собой 4 символа, идущих подряд. 
Каждый символ представляет собой команду для платформ в заданном порядке: верх, право, низ, лево.

Данная последовательность преобразуется в один байт, где на каждую команду отводится 2 бита.

Каждая команда для платформы принимает следующие значения:

| Символ | Бинарное представление | Описание              |
|:-------|:-----------------------|:----------------------|
| `0`    | `00`                   | Сигнал отсоединения   | 
| `1`    | `01`                   | Сигнал присоединения  |
| `x`    | `10`                   | Неопределенный сигнал |

Пример: `x10x --> 10_01_00_10`

## Формат бинарного файла

Бинарный файл состоит из двух частей: из заголовка и тела файла.

### Заголовок

Заголовок состоит из множества идущих подряд команд по 4 байта. 
В начале и в конце обозначается зарезервированными командами.

При чтении команды в `uint` и разбиении на байте самый старший байт будет являться идентификатором модуля, 
а остальные 3 байта - сдвигом относительно *тела* файла, после которого идет начало этого модуля.
Последний модуль имеет идентификатор 255 (`FF`) и указывает длину тела файла.

### Тело

Тело файла состоит из подряд идущих команд размером 1 байт, следом за которыми идут их аргументы.
Первой командой идет команда начала модуля, после которой идут команды для этого модуля.
В конце следует команда конца модуля, после которой будет команда начала следующего модуля (если есть).

## Бинарные команды

Ниже представлены бинарные команды, которые генерирует программа

| Команда (*HEX*) | Описание                    | Аргумент |
|:----------------|:----------------------------|----------|
| `A9_B8_C7_D6`   | Начало заголовка            | -        |
| `00_00_00_00`   | Конец заголовка             | -        |
| `10`            | Управление углом Q1         | 4 байт   | 
| `20`            | Управление углом Q2         | 4 байт   |
| `30`            | Сигнал для платформ         | 1 байт   |
| `40`            | Конец блока команд          | -        |
| `50`            | Повторять следующие команды | -        |
| `A0`            | Начало команд модуля        | 1 байт   |
| `A1`            | Конец команд модуля         | -        |
