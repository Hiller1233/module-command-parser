﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ModuleCommandParser;

public class ConfigurationScript : IDisposable
{
    public byte ModuleId;
    public bool IsModuleSet;
    public readonly FileInfo Output;
    public readonly string ScriptName;
    public readonly Dictionary<byte, uint> Modules;
    public readonly BinaryWriter Writer;

    private readonly MemoryStream _memoryStream;

    public ConfigurationScript(FileInfo output, string scriptName)
    {
        Output = output;
        ScriptName = scriptName;
        Modules = new Dictionary<byte, uint>(4);
        _memoryStream = new MemoryStream(512);
        Writer = new BinaryWriter(_memoryStream);
    }

    public uint Length => (uint)_memoryStream.Length;

    public void Dispose()
    {
        Writer?.Dispose();
        GC.SuppressFinalize(this);
    }
}