﻿using System;
using System.IO;

namespace ModuleCommandParser;

public static class Program
{ 
    public static void Main(string[] args)
    {
        try
        {
            Start(args);
        }
        catch (StopProgramException e)
        {
            Console.WriteLine(e.Message);
            Environment.Exit(1);
        }
#pragma warning disable CS0168
        catch (Exception e)
#pragma warning restore CS0168
        {
            Console.WriteLine("Ошибка во время выполнения программы");
#if DEBUG
            throw;
#else
            Console.WriteLine(e.Message);
            Environment.Exit(1);
#endif
        }
    }

    private static void Start(string[] args)
    {
        var (input, output) = ParseArgs(args);
        using var commandParser = new CommandParser(input, output);
        commandParser.Parse();
    }

    private static (FileInfo input, DirectoryInfo output) ParseArgs(string[] args)
    {
        if (args.Length == 0)
        {
            throw new StopProgramException("Ожидается как минимум один аргумент с входным файлом");
        }

        var input = new FileInfo(args[0]);
        if (!input.Exists)
        {
            throw new StopProgramException($"Файл {input} не найден");
        }

        var outputPath = args.Length > 1 ? args[1] : Path.Combine(input.DirectoryName ?? "", "output");
        var output = new DirectoryInfo(outputPath);
        if (!output.Exists)
        {
            output.Create();
        }

        return (input, output);
    }
}