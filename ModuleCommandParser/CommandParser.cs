﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ModuleCommandParser.Commands;

namespace ModuleCommandParser;

public class CommandParser : IDisposable
{
    private ConfigurationScript _script;
    private readonly FileInfo _fileInput;
    private readonly DirectoryInfo _dirOutput;
    private readonly IReadOnlyDictionary<string, Command> _commandMap;

    public CommandParser(FileInfo fileInput, DirectoryInfo dirOutput)
    {
        _fileInput = fileInput;
        _dirOutput = dirOutput;
        
        _commandMap = CommandBuilder.Create()
            .AddCommand(KnownCommands.RotateQ1, "rotate1", "r1").WithFloatArgument()
            .AddCommand(KnownCommands.RotateQ2, "rotate2", "r2").WithFloatArgument()
            .AddCommand(KnownCommands.Connect, "connect", "cn").WithConnectionArgument()
            .AddCommand(KnownCommands.Block, "block", "b")
            .AddCommand(KnownCommands.Loop, "repeat", "loop")
            .AddSpecialCommand(StartScript, "configuration", "conf")
            .AddSpecialCommand(StartModule, "id", "module")
            .Build();
    }

    public void Parse()
    {
        var content = File.ReadAllLines(_fileInput.FullName);
        if (content.Length == 0)
        {
            throw new StopProgramException("Переданный файл не содержит команд");
        }

        foreach (var token in content)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                continue;
            }
            
            var split = token.ToLower().Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            if (split.Length is 0 or > 2)
            {
                throw new StopProgramException($"Не удалось прочитать токен: {token}");
            }

            if (!_commandMap.TryGetValue(split[0], out var command))
            {
                throw new StopProgramException($"Неизвестная команда: {split[0]}");
            }

            if (command.IsSpecial)
            {
                command.SpecialAction(split);
                continue;
            }
            
            if (_script == null || !_script.IsModuleSet)
            {
                throw new StopProgramException("Для записи команд необходимо указать скрипт и модуль");
            }

            _script.Writer.Write(command.Byte);
            if (!command.HasArgument)
            {
                continue;
            }
            
            if (split.Length != 2)
            {
                throw new StopProgramException($"Команда {split[0]} принимает один аргумент");
            }

            command.ArgumentWriter(_script.Writer, split[1]);
        }
        
        WriteCurrentScript();
    }
    
    public void Dispose()
    {
        _script?.Dispose();
        GC.SuppressFinalize(this);
    }

    private void StartScript(string[] tokenSplit)
    {
        WriteCurrentScript();
        InitializeScript(tokenSplit);
    }

    private void WriteCurrentScript()
    {
        if (_script == null)
        {
            return;
        }

        if (_script.Length == 0 || _script.Modules.Count == 0)
        {
            throw new StopProgramException("Не удалось записать скрипт, требуется минимум 1 модуль");
        }

        _script.Writer.Write(KnownCommands.ModuleEnd);
        using var fileStream = _script.Output.Create();
        WriteHeaders(fileStream);
        _script.Writer.BaseStream.Seek(0, SeekOrigin.Begin);
        _script.Writer.BaseStream.CopyTo(fileStream);
        fileStream.Flush();
        Console.WriteLine($"Скрипт записан в файл {_script.Output.FullName}");
        _script.Dispose();
        _script = null;
    }

    private void WriteHeaders(Stream stream)
    {
        using var headerWriter = new BinaryWriter(stream, Encoding.UTF8, true);
        var scriptLength = _script.Length;
        if ((scriptLength & 0x_FF_00_00_00) != 0)
        {
            throw new StopProgramException("Скрипт содержит слишком большое количество команд");
        }

        _script.Modules[0x_FF] = scriptLength;
        headerWriter.Write(KnownCommands.HeaderStart);
        foreach (var (id, offset) in _script.Modules.OrderBy(x => x.Value))
        {
            var headerBytes = ((uint)id << 24) | offset;
            headerWriter.Write(headerBytes);
        }
        
        headerWriter.Write(KnownCommands.HeaderEnd);
        headerWriter.Flush();
    }

    private void InitializeScript(string[] tokenSplit)
    {
        if (tokenSplit.Length != 2)
        {
            throw new StopProgramException("Для создания скрипта необходимо задать его имя");
        }

        var scriptName = tokenSplit[1];
        var targetFile = Path.Combine(_dirOutput.FullName, scriptName);
        if (File.Exists(targetFile))
        {
            targetFile += "_" + DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        _script = new ConfigurationScript(new FileInfo(targetFile), scriptName);
    }

    private void StartModule(string[] tokenSplit)
    {
        if (tokenSplit.Length != 2 || !byte.TryParse(tokenSplit[1], out _script.ModuleId))
        {
            throw new StopProgramException($"Не удалось прочитать идентификатор модуля: {tokenSplit[1]}");
        }
        
        if (_script.ModuleId is 0 or 255 || _script.Modules.ContainsKey(_script.ModuleId))
        {
            throw new StopProgramException($"Идентификатор модуля должен быть уникальным и не равнятся 0 или 255: {_script.ModuleId}");
        }

        if (_script.IsModuleSet)
        {
            _script.Writer.Write(KnownCommands.ModuleEnd);
        }
        
        _script.IsModuleSet = true;
        _script.Modules[_script.ModuleId] = _script.Length;
        _script.Writer.Write(KnownCommands.ModuleStart);
        ArgumentWriters.WriteByte(_script.Writer, tokenSplit[1]);
    }
}