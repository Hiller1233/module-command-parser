﻿using System;

namespace ModuleCommandParser;

public class StopProgramException : Exception
{
    public StopProgramException(string message) : base(message)
    {
    }
}