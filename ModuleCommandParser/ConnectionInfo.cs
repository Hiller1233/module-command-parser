﻿namespace ModuleCommandParser;

public readonly struct ConnectionInfo
{
    public byte Byte { get; }

    private ConnectionInfo(byte data)
    {
        Byte = data;
    }

    public static bool TryParse(string input, out ConnectionInfo info)
    {
        info = new ConnectionInfo();
        if (input is not { Length: 4 })
        {
            return false;
        }

        var result = 0;
        foreach (var c in input)
        {
            var delta = c switch
            {
                '0' => 0b_00,
                '1' => 0b_01,
                'x' => 0b_10,
                _ => int.MinValue
            };

            if (delta == int.MinValue)
            {
                return false;
            }

            result <<= 2;
            result |= delta;
        }

        info = new ConnectionInfo((byte)result);
        return true;
    }
}