﻿using System.Globalization;
using System.IO;

namespace ModuleCommandParser.Commands;

public static class ArgumentWriters
{
    public static void WriteFloat(BinaryWriter writer, string input)
    {
        if (!float.TryParse(input, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out var arg))
        {
            throw new StopProgramException($"Не удалось прочитать аргумент типа float: {input}");
        }

        writer.Write(arg);
    }
    
    public static void WriteByte(BinaryWriter writer, string input)
    {
        if (!byte.TryParse(input, out var arg))
        {
            throw new StopProgramException($"Не удалось прочитать аргумент типа byte: {input}");
        }

        writer.Write(arg);
    }
    
    public static void WriteConnectionInfo(BinaryWriter writer, string input)
    {
        if (!ConnectionInfo.TryParse(input, out var arg))
        {
            throw new StopProgramException($"Не удалось прочитать аргумент типа connection: {input}");
        }

        writer.Write(arg.Byte);
    }
}