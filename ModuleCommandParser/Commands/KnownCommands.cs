﻿namespace ModuleCommandParser.Commands;

public static class KnownCommands
{
    /// <summary>
    /// Угол Q1
    /// Далее следует угол float
    /// </summary>
    public static byte RotateQ1 => 0x_10;
    
    /// <summary>
    /// Угол Q2
    /// Далее следует угол float
    /// </summary>
    public static byte RotateQ2 => 0x_20;
    
    /// <summary>
    /// Послать сигнал на платформу
    /// Далее следует по 2 бита для каждой из 4 плафторм
    /// </summary>
    public static byte Connect => 0x_30;
    
    /// <summary>
    /// Конец блока команд
    /// </summary>
    public static byte Block => 0x_40;
    
    /// <summary>
    /// Повторить все команды после этой
    /// </summary>
    public static byte Loop => 0x_50;
    
    /// <summary>
    /// Начало команд для модуля
    /// Далее следует 1 байт идентификатора модуля
    /// </summary>
    public static byte ModuleStart => 0x_A0;
    
    /// <summary>
    /// Конец команд модуля
    /// </summary>
    public static byte ModuleEnd => 0x_A1;
    
    /// <summary>
    /// Начало заголовка
    /// </summary>
    public static uint HeaderStart => 0x_A9_B8_C7_D6;
    
    /// <summary>
    /// Конец заголовка
    /// </summary>
    public static uint HeaderEnd => 0x_00_00_00_00;
}