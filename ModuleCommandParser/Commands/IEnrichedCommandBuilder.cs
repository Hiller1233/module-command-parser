﻿namespace ModuleCommandParser.Commands;

public interface IEnrichedCommandBuilder : ICommandBuilder
{
    public ICommandBuilder WithFloatArgument();
    
    public ICommandBuilder WithByteArgument();
    
    public ICommandBuilder WithConnectionArgument();
}