﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ModuleCommandParser.Commands;

public class CommandBuilder : IEnrichedCommandBuilder
{
    private byte? _configuredCommand;
    private string[] _configuredAllies;
    private Dictionary<string, Command> _commands;
    
    private CommandBuilder()
    {
        _commands = new Dictionary<string, Command>();
    }

    public IReadOnlyDictionary<string, Command> Build()
    {
        CreateStoredCommand();
        var dict = _commands;
        _commands = null;
        return dict;
    }

    public IEnrichedCommandBuilder AddCommand(byte num, params string[] allies)
    {
        CreateStoredCommand();
        _configuredCommand = num;
        _configuredAllies = allies;
        return this;
    }

    public ICommandBuilder AddSpecialCommand(Action<string[]> specialAction, params string[] allies)
    {
        CreateStoredCommand();
        RegisterCommand(new Command(specialAction), allies);
        return this;
    }

    public ICommandBuilder WithFloatArgument()
    {
        CreateStoredCommand(ArgumentWriters.WriteFloat);
        return this;
    }

    public ICommandBuilder WithByteArgument()
    {
        CreateStoredCommand(ArgumentWriters.WriteByte);
        return this;
    }

    public ICommandBuilder WithConnectionArgument()
    {
        CreateStoredCommand(ArgumentWriters.WriteConnectionInfo);
        return this;
    }

    private void CreateStoredCommand(Action<BinaryWriter, string> argumentWriter = null)
    {
        if (_configuredCommand == null)
        {
            return;
        }

        var command = new Command(_configuredCommand.Value, argumentWriter);
        RegisterCommand(command, _configuredAllies);
        _configuredCommand = null;
        _configuredAllies = null;
    }

    private void RegisterCommand(Command command, string[] allies)
    {
        if (allies == null || allies.Length == 0)
        {
            throw new ArgumentException("Должно быть задано хотя бы одно имя команды", nameof(allies));
        }

        foreach (var ally in allies)
        {
            _commands[ally] = command;
        }
    }
    
    public static ICommandBuilder Create() => new CommandBuilder();
}