﻿using System;
using System.IO;

namespace ModuleCommandParser.Commands;

public class Command
{
    public Action<BinaryWriter, string> ArgumentWriter { get; }

    public byte Byte { get; }
    
    public Action<string[]> SpecialAction { get; }
    
    public bool IsSpecial { get; }
    
    public bool HasArgument { get; }

    public Command(byte num, Action<BinaryWriter, string> argumentWriter = null)
    {
        Byte = num;
        ArgumentWriter = argumentWriter;
        HasArgument = argumentWriter != null;
    }

    public Command(Action<string[]> specialAction)
    {
        SpecialAction = specialAction;
        IsSpecial = true;
    }
}