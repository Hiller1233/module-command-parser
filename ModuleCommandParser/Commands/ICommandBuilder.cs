﻿using System;
using System.Collections.Generic;

namespace ModuleCommandParser.Commands;

public interface ICommandBuilder
{
    public IReadOnlyDictionary<string, Command> Build();
    
    public IEnrichedCommandBuilder AddCommand(byte num, params string[] allies);
    
    public ICommandBuilder AddSpecialCommand(Action<string[]> specialAction, params string[] allies);
}